# memo

## 2-4
```
oc new-project dev
cd bookinfo-manifests/sample/
oc apply -f bookinfo/bookinfo-ratings.yaml
oc apply -f bookinfo/bookinfo-details.yaml
oc apply -f bookinfo/bookinfo-productpage.yaml
oc apply -l version!=v2,version!=v3 -f bookinfo/bookinfo-reviews.yaml
oc adm policy add-scc-to-user anyuid -z bookinfo-reviews
```

```
export CURL_IMAGE="governmentpaas/curl-ssl"
oc run -it curl --image=${CURL_IMAGE} --rm --restart=Never --command - curl -s productpage:9080/productpage
```

```
oc expose svc productpage --port=9080
```

```
oc apply -l version=v2 -f bookinfo/bookinfo-reviews.yaml 
```

## 3-2
- Tekton pipline cli
```
sudo dnf copr enable chmouel/tektoncd-cli
sudo dnf install tektoncd-cli
tkn version
```

- After PVC creation, if Pod specifies it then PV will be created by dynamic-provisioning.
```
oc apply -f tekton/volumesources/bookinfo-pvc.yaml
oc apply -f tekton/tasks/git-clone.yaml
oc apply -f tekton/tasks/source-lister.yaml
```

## 3-3
```
tkn task start gradle-build --showlog --workspace name=output,claimName=bookinfo --param='contextDir=reviews/reviews-application'
```
```
[...]
[gradle-build] Starting a Gradle Daemon (subsequent builds will be faster)
[gradle-build] > Task :reviews-application:compileJava
[gradle-build] > Task :reviews-application:processResources NO-SOURCE
[gradle-build] > Task :reviews-application:classes
[gradle-build] > Task :reviews-application:war
[gradle-build] > Task :reviews-application:assemble
[gradle-build] > Task :reviews-application:compileTestJava
[gradle-build] > Task :reviews-application:processTestResources NO-SOURCE
[gradle-build] > Task :reviews-application:testClasses
[gradle-build] > Task :reviews-application:test
[gradle-build] > Task :reviews-application:check
[gradle-build] > Task :reviews-application:build
[gradle-build] 
[gradle-build] BUILD SUCCESSFUL in 34s
[gradle-build] 4 actionable tasks: 4 executed

[ls-archive] total 8
[ls-archive] -rw-r--r--    1 root     10006300      7582 Nov 29 14:45 reviews-application-1.0.war
```

## 4-2
```
oc apply -f ./volumesources/bookinfo-pvc.yaml
oc create -f ./volumesources/bookinfo-manifests-pvc.yaml
```

```
tkn hub install task git-clone --version 0.3
```

## 4-4
```
tkn hub install task conftest --version 0.1
```

## 4-5
```
tkn hub install task kaniko --version 0.4
oc adm policy add-scc-to-user anyuid -z tekton-admin
```